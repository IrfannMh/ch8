'use strict';

var utils = require('../utils/writer.js');
var Biodata = require('../service/BiodataService');

module.exports.getBiodata = function getBiodata (req, res, next, biodataId) {
  Biodata.getBiodata(biodataId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateBiodata = function updateBiodata (req, res, next, body, biodataId) {
  Biodata.updateBiodata(body, biodataId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
