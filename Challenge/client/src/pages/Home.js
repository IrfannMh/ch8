import React from 'react'
import DefaultLayout from '../layouts/default'
import Menu from "../components/Menu";
import Data from '../opponents.json'
import Level from '../components/Level'
import Opponent from '../components/CardPlayer'

const Home = () => (
  <DefaultLayout>
    <Menu/>
    <Level/>
    <Opponent dataUser={Data}/>
  </DefaultLayout>
)

export default Home;