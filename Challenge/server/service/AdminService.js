'use strict';


/**
 * Get Rooms
 * Get all Rooms
 *
 * returns inline_response_200_3
 **/
exports.getAllRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "gameId" : 1,
    "nameRoom" : "room 1",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "isOpen"
  }, {
    "gameId" : 1,
    "nameRoom" : "room 1",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "isOpen"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all biodatas
 * Get all biodatas player in database
 *
 * returns inline_response_200_5
 **/
exports.getBiodatas = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "phoneNumber" : "81223334444",
    "address" : "Jakarta",
    "avatarURL" : "https://www.google.com/search?q=swagger&sxsrf=M",
    "fullName" : "User 1 Rock Paper Scissor",
    "bio" : "Best player game rock paper scissor"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get All Game
 * get all game
 *
 * returns List
 **/
exports.getGame = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "roundCount" : 3,
  "winner" : "player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "isFinished" : true
}, {
  "roundCount" : 3,
  "winner" : "player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "isFinished" : true
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get All Round
 * Get All round
 *
 * returns List
 **/
exports.getRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "winnerRound" : "Player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "player2Option" : "Scissor",
  "player1Option" : "Rock"
}, {
  "winnerRound" : "Player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "player2Option" : "Scissor",
  "player1Option" : "Rock"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all User
 * Get all User in database
 *
 * returns List
 **/
exports.getUsers = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "Player" : {
    "password" : "12345678",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "username" : "user person 1"
  },
  "status" : "OK"
}, {
  "Player" : {
    "password" : "12345678",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "username" : "user person 1"
  },
  "status" : "OK"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

