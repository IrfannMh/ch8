const DefaultLayout = ({ children }) => (
  <div>
    <header>
      <div className="color-overlay">
        {children}
      </div>
    </header>
  </div>
);
  
export default DefaultLayout;
  