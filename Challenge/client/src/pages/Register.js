import React from 'react';
import DefaultLayout from '../layouts/default'
// import {Form, Button} from "react-bootstrap";

import RegisterForm from '../components/FormRegister'

const Register= () => (
  <DefaultLayout>
        <RegisterForm/>
  </DefaultLayout>
)

export default Register;