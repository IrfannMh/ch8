import React from 'react'
import DefaultLayout from '../layouts/default'
import LoginForm from '../components/LoginForm'

const Login = () => (
  <DefaultLayout>
    <LoginForm/>
  </DefaultLayout>
)

export default Login;