import logo from "../assets/images/logo.svg";
import { Link } from "react-router-dom";
import DefaultLayout from "../layouts/default";

const NotFound = () => (
  <DefaultLayout>
    <div className="App-header-container">
      <Link to="/">
        <img src={logo} className="App-logo App-logo--small" alt="logo" />
      </Link>
      <h1>404</h1>
    </div>
  </DefaultLayout>
);

export default NotFound;