import React from 'react'

import './LoginForm.css'

import {Form, Button} from "react-bootstrap";

export default function FormRegister() {
    return (
        <div className='flex'>
            <Form className="rounded p-5 p-sm-3">
          <div className='m-4'>
            <h4 className='mb-4 fw-bold'> REGISTER</h4>
            <Form.Group className="mb-3" controlId="formBasicUsername">
              <Form.Control
                type='text'
                placeholder="Insert your Username"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control
                type='password'
                placeholder="Insert your Password"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Control
                type='password'
                placeholder="Confirm Password"
              />
            </Form.Group>
            <div className='d-grid mb-5'>
              <Button variant="warning" className='text-white' type="submit">
                Register
              </Button>
            </div>
          </div>
        </Form>
        </div>
    )
}
