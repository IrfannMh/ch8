'use strict';


/**
 * Get all Player
 * Get all Player in database
 *
 * point Integer Filter Player by Point (optional)
 * level String Filter Player by Level (optional)
 * returns List
 **/
exports.getPlayers = function(point,level) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "Player" : {
    "password" : "12345678",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "username" : "user person 1"
  },
  "status" : "OK"
}, {
  "Player" : {
    "password" : "12345678",
    "updated_at" : "30-07-2021 12:00 +07:00",
    "created_at" : "30-07-2021 12:00 +07:00",
    "id" : 1,
    "username" : "user person 1"
  },
  "status" : "OK"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

