'use strict';


/**
 * get game by user Id
 * get all game by user
 *
 * userId Integer 
 * returns List
 **/
exports.gameUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "roundCount" : 3,
  "winner" : "player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "isFinished" : true
}, {
  "roundCount" : 3,
  "winner" : "player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "isFinished" : true
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Player Make Choice
 * player choose paper, scissor, or rock
 *
 * body String 
 * roomId Integer 
 * returns inline_response_200_1
 **/
exports.postOption = function(body,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "game" : {
    "player2Choose" : "waiting player 2",
    "player1Choose" : "Rock",
    "roomName" : "Room 1"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

