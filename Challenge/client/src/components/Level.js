import React from 'react'
import {Container, Button} from 'react-bootstrap'
import './Level.css'
export default function Level() {
  return (
    <div className='margin'>
      <Container>
        <h1>Choose your opponent</h1>
        <Button className='buttonLevel' variant='secondary' type="button">Novice</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Class A</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Class B</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Class C</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Class D</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Candidate Master</Button>{' '}
        <Button className='buttonLevel' variant='secondary' type="button">Grand Master</Button>{' '}
      </Container>
    </div>
  )
}
