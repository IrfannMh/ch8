'use strict';


/**
 * Get Round by user Id
 * Get All round by user Id
 *
 * userId Integer 
 * returns List
 **/
exports.roundUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "winnerRound" : "Player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "player2Option" : "Scissor",
  "player1Option" : "Rock"
}, {
  "winnerRound" : "Player 1",
  "finished_at" : "30-07-2021 12:00 +07:00",
  "created_at" : "30-07-2021 12:00 +07:00",
  "player2Option" : "Scissor",
  "player1Option" : "Rock"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

