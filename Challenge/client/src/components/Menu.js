import React from 'react'
import {Navbar, Nav, Container} from "react-bootstrap";

const Menu = () => (
  <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed='top'>
    <Container>
    <Navbar.Brand href="/">Suit Games</Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className='ms-auto'>
        <Nav.Link className='fs-5' href="/register">Register</Nav.Link>
        <Nav.Link className='fs-5' href="/login">Login</Nav.Link>
      </Nav>
    </Navbar.Collapse>
    </Container>
  </Navbar>
)

export default Menu
